#!/bin/sh

[ -z $PASSSPOTIFYNAME ] && PASSSPOTIFYNAME="spotify"

# get spotify logins from pass
splogin=$(pass spotify | grep "login:" | sed -s "s/login:\ \(.\+\)/\1/g")   
sppass=$(pass spotify | head -n 1)   
spclientid=$(pass spotify | grep "client_id:" | sed -s "s/client_id:\ \(.\+\)/\1/g")   
spclientsecret=$(pass spotify | grep "client_secret:" | sed -s "s/client_secret:\ \(.\+\)/\1/g")   

# Replace with credentials and writeto mopidy.conf
sed "s/<spotify-login>/$splogin/g" ./mopidy-def.conf |
    sed  "s/<spotify-password>/$sppass/g" |
    sed  "s/<spotify-client-id>/$spclientid/g" |
    sed  "s/<spotify-client-secret>/$spclientsecret/g" |
    tee  ./mopidy.conf
