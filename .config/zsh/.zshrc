autoload -U colors && colors
PS1="%B%F{blue}>%f%b "
RPROMPT="%F{008}%~%f"

HISTSIZE=10000
SAVEHIST=10000
HISTFILE=$XDG_CONFIG_HOME/zsh/history

fpath+=$XDG_CONFIG_HOME/zsh/completions
autoload -Uz compinit && compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

bindkey -v
export KEYTIMEOUT=1

# Dissable execute-named-cmd:
bindkey -M vicmd -r ":"

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# Load aliases and shortcuts if existent.
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"

source $XDG_CONFIG_HOME/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $XDG_CONFIG_HOME/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

#PYV virtual environments minimalism 26-04-2022
source $HOME/.local/bin/pyv

# Adding a fetch

ufetch

# zsh optional sources

export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"


# Make git comply with XDG home standarts
export GIT_CONFIG_GLOBAL=$XDG_CONFIG_HOME/git/config


# expose pass
export PASSWORD_STORE_EXPOSE_SUBDIR="Σxposed"

# custom completions
compdef pexpose=pass

fpath=($XDG_CONFIG_HOME/zsh/completions $fpath)

# Cuda ld libraries fix
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:
# export LD_LIBRARY_PATH=/opt/cuda/lib64 ${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
export PATH=$PATH:$HOME/.opam/default/bin

[ -f "/home/kiko/.ghcup/env" ] && source "/home/kiko/.ghcup/env" # ghcup-env
