laptop_rule = {
  matches = {
    {
      { "node.name", "equals", "alsa_output.pci-0000_00_1f.3-platform-skl_hda_dsp_generic.HiFi__hw_sofhdadsp__sink" },
    },
  },
  apply_properties = {
    ["node.description"] = "Laptop",
    ["node.nick"] = " 󰓃",
  }
}

dac_rule = {
  matches = {
    {
      { "node.name", "equals", "alsa_output.usb-FiiO_FiiO_BTR5-00.analog-stereo" },
    },
  },
  apply_properties = {
    ["node.description"] = "Fiio BTR5",
    ["node.nick"] = " 󰀰",
  },
}


audio_over_disp_rule = {
  matches = {
    {
      { "node.name", "equals", "alsa_output.pci-0000_00_1f.3-platform-skl_hda_dsp_generic.HiFi__hw_sofhdadsp_5__sink" },
    },
    {
      { "node.name", "equals", "alsa_output.pci-0000_00_1f.3-platform-skl_hda_dsp_generic.HiFi__hw_sofhdadsp_4__sink" },
    },
    {
      { "node.name", "equals", "alsa_output.pci-0000_00_1f.3-platform-skl_hda_dsp_generic.HiFi__hw_sofhdadsp_3__sink" },
    },
    {
      { "node.name", "equals", "alsa_output.pci-0000_01_00.1.hdmi-stereo" },
    },
  },
  apply_properties = {
    ["node.disabled"] = true,
  },
}


table.insert(alsa_monitor.rules, laptop_rule)
table.insert(alsa_monitor.rules, dac_rule)
table.insert(alsa_monitor.rules, audio_over_disp_rule)
